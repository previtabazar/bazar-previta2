'use strict';

/**
 * @ngdoc function
 * @name bazarApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the bazarApp
 */
angular.module('bazarApp')
  .controller('AboutCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
