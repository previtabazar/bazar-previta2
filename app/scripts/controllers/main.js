'use strict';

/**
 * @ngdoc function
 * @name bazarApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the bazarApp
 */
angular.module('bazarApp')
  .controller('MainCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
